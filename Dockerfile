FROM athackst/ros2:foxy-dev

ENV UBUNTU_DISTRO focal

RUN apt-get update \
  && apt-get install -y software-properties-common \
  && sudo apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE \
  || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE \
  && sudo add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo $UBUNTU_DISTRO main" -u \
  && sudo apt-get update \
  && sudo apt-get install -y \
    librealsense2-dkms \
    librealsense2-utils \
    librealsense2-dev \
    librealsense2-dbg

#RUN mkdir -p /ros_realsense2/src \
#  && cd /ros_realsense2/src/ \
#  && git clone https://github.com/IntelRealSense/realsense-ros.git -b foxy \
#  && cd /ros_realsense2 \
#  && rosdep update \
#  && rosdep install -i --from-path src --rosdistro $ROS_DISTRO -y \
#  && colcon build
